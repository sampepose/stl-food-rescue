var squel = require('squel');
var util = require('./util');
var SendGrid = require('sendgrid').SendGrid;
var Email = require('sendgrid').Email;
var moment = require('moment');

var sendgrid = new SendGrid(process.env.SENDGRID_USERNAME,
    process.env.SENDGRID_PASSWORD);
var queryResponse = util.queryResponse;

var api = {
    getFoodRuns:function (res) {
        var qs = "SET time zone -5; SELECT fr.food_run_id, fr.pick_up_location, fr.drop_off_location, fr.time, sum(volunteer_amount) AS total_volunteer_amount, sum(car_amount) AS total_car_amount " +
            "FROM food_runs fr " +
            "LEFT JOIN volunteers_food_runs_map map " +
            "ON fr.food_run_id = map.food_run_id " +
            "GROUP BY fr.food_run_id ORDER BY fr.food_run_id;";
        queryResponse(qs, [], res);
    },
    getFoodRun:function (id, res) {
        var qs = "SET time zone -5; SELECT fr.food_run_id, fr.pick_up_location, fr.drop_off_location, fr.time, sum(volunteer_amount) AS total_volunteer_amount, sum(car_amount) AS total_car_amount " +
            "FROM food_runs fr " +
            "LEFT JOIN volunteers_food_runs_map map " +
            "ON fr.food_run_id = map.food_run_id " +
            "WHERE fr.food_run_id = $1 " +
            "GROUP BY fr.food_run_id ORDER BY fr.food_run_id;";
        queryResponse(qs, [id], res);
    },
    getFoodRunsFiltered:function (week, res) {
        var qs = "SET time zone -5; SELECT fr.food_run_id, fr.pick_up_location, fr.drop_off_location, fr.time, sum(volunteer_amount) AS total_volunteer_amount, sum(car_amount) AS total_car_amount " +
            "FROM food_runs fr " +
            "LEFT JOIN volunteers_food_runs_map map " +
            "ON fr.food_run_id = map.food_run_id " +
            "WHERE date_part('week', time) =" +
                "(CASE WHEN (date_part('week', now()) + " + week + " > 52)" +
                    " THEN date_part('week', now()) + " + week + " - 52" +
                    " ELSE date_part('week', now()) + " + week + " END)" +
            " GROUP BY fr.food_run_id ORDER BY fr.time;";
        queryResponse(qs, [], res);
    },
    addFoodRun:function (fields, res) {
        var data = [], n = 1;
        var qs = squel.insert({usingValuePlaceholders:true})
            .into('food_runs');
        for (var k in fields) {
            if (isNaN(fields[k]) || fields[k] === '') {
                data.push(fields[k]);
                qs.set(k, "$" + n++);
            } else {
                data.push(parseInt(fields[k]));
                qs.set(k, "$" + n++);
            }
        }
        qs = qs.toString() + " RETURNING *";
        queryResponse(qs, data, res);
    },
    removeFoodRun:function (id, res) {
        var qs = "DELETE FROM food_runs WHERE food_run_id=$1";
        queryResponse(qs, [id], res);
    },
    editFoodRun:function (id, fields, res) {
        //TODO: Do this...
        var data = [id], n = 2;
        var qs = squel.update({usingValuePlaceholders:true})
            .table('tasks')
            .where('task_id=$1');
        for (var k in fields) {
            if (fields[k] == null) {
                qs.set(k, "NULL");
            } else if (isNaN(fields[k]) || fields[k] === '') {
                data.push(fields[k]);
                qs.set(k, "$" + n++);
            } else {
                data.push(parseInt(fields[k]));
                qs.set(k, "$" + n++);
            }
        }
        qs = qs.toString();
        queryResponse(qs, data, res);
    },
    getTotalVolunteerAmount:function (id, res) {
        var qs = "SELECT sum(volunteer_amount) FROM volunteers_food_runs_map WHERE food_run_id=$1";
        queryResponse(qs, [id], res);
    },
    getTotalCarAmount:function (id, res) {
        var qs = "SELECT sum(car_amount) FROM volunteers_food_runs_map WHERE food_run_id=$1";
        queryResponse(qs, [id], res);
    },
    addVolunteerAssignment:function (id, fields, res) {
        queryResponse("SELECT sum(car_amount) AS totalcars, sum(volunteer_amount) AS totalvolunteers FROM volunteers_food_runs_map WHERE food_run_id=$1", [id], res,
            function (res, affected_rows, data) {
                var totalCars = affected_rows[0]['totalcars'];
                var totalVolunteers = affected_rows[0]['totalvolunteers'];
                if (totalVolunteers >= 5 && totalCars >= 3) {
                    res.writeHead(200, {'Content-Type':'text/plain'});
                    res.write("{\"error\" : \"AlreadyFull\"}");
                    res.end();
                } else if (totalVolunteers + parseInt(fields['volunteer_amount']) > 5 || totalCars + parseInt(fields['car_amount']) > 3) {
                    res.writeHead(200, {'Content-Type':'text/plain'});
                    res.write("{\"error\" : \"TooMany\", \"volunteer\" : " + (5 - totalVolunteers) + ", \"car\" : " + (3 - totalCars) + " }");
                    res.end();
                } else {
                    var qs = "INSERT INTO volunteers_food_runs_map(volunteer_id, food_run_id, volunteer_amount, car_amount) " +
                        "VALUES ((SELECT volunteer_id FROM volunteers WHERE email ilike $2), $1, $3 ,$4) RETURNING *";
                    var data = [id];
                    for (var k in fields) {
                        data.push(fields[k]);
                    }
                    queryResponse(qs, data, res,
                        function (res, affected_rows, data) {
                            res.writeHead(200, {'Content-Type':'text/plain'});
                            if (affected_rows.length == 1)
                                res.write(JSON.stringify(affected_rows[0]));
                            else
                                res.write(JSON.stringify(affected_rows));
                            res.end();
                            queryResponse("SELECT time, drop_off_location, pick_up_location FROM food_runs WHERE food_run_id = $1", [id], res,
                                function (res, affected_rows) {
                                    var mail = new Email({
                                        to:fields['email'],
                                        from:'info@stlfoodrescue.org',
                                        subject:'St. Louis Food Rescue Food-Run Confirmation'
                                    });
                                    mail.addSubVal('-time-', moment(affected_rows[0]['time']).format("dddd, MMMM Do, YYYY [at] h:mma"));
                                    mail.addSubVal('-people-', fields['volunteer_amount']);
                                    mail.addSubVal('-cars-', fields['car_amount']);
                                    var drop, pick;
                                    switch (affected_rows[0]['drop_off_location']) {
                                        case 2:
                                            drop = "Salvation Army";
                                            break;
                                        case 3:
                                            drop = "New Life";
                                    }
                                    switch (affected_rows[0]['pick_up_location']) {
                                        case 2:
                                            pick = "Whole Foods";
                                            break;
                                    }
                                    mail.addSubVal('-dropoff-', drop);
                                    mail.addSubVal('-pickup-', pick);
                                    mail.html = 'Thank you for signing up to volunteer with St. Louis Food Rescue on <strong>-time-</strong>! You have specified that you will be bringing <strong>-people- volunteers</strong> and <strong>-cars- vehicles</strong>! You will be picking the food up from <strong>-pickup-</strong> and delivering the food to <strong>-dropoff-</strong>. You can find the addresses of these locations on our website.<br /><br />' +
                                        'Please arrive at the back of Whole Foods by 9:00pm with your SUV, minivan, or truck. Be sure to place a tarp in your vehicle so that in the event that a rotten item is in your vehicle, it does not damage or stain the interior. Sometimes the bread takes a few minutes to be packaged, but that is very uncommon. Please be sure to check the dairy cooler for a small cart labeled "Food Bank" with dairy products that will hit their expiration dates soon.<br /><br />' +
                                        'Please make safety a top priority when volunteering. Although we have no requirements, we highly suggest that you wear close-toed shoes in case a box breaks or falls when loading.<br /><br />' +
                                        'Although we have previously distributed food to the homeless after unloading, St. Louis Food Rescue asks for the time being that you do not. New Life sometimes asks for paperwork to be filled out, but if you feel unsafe leaving your car parked and entering the building, please do not honor their request.<br /><br />' +
                                        'Please do not hesitate to contact our Volunteer Coordinator, Will Hopkins, at (314) 445-7273 if you have any questions.<br /><br />' +
                                        'Thank you for helping us feed the hungry and reduce food waste!'
                                    sendgrid.send(mail, function (success, message) {
                                        if (!success) {
                                            util.log(message);
                                        }
                                    });
                                });
                        },
                        function (error, res) {
                            api.editVolunteerAssignment(id, fields, res);
                        });
                }
            });
    },
    editVolunteerAssignment:function (id, fields, res) {
        var qs = "UPDATE volunteers_food_runs_map " +
            "SET volunteer_amount=$3, car_amount=$4 WHERE volunteer_id=(SELECT volunteer_id FROM volunteers WHERE email ilike $2) AND food_run_id=$1";
        var data = [id];
        for (var k in fields) {
            data.push(fields[k]);
        }
        queryResponse(qs, data, res, function (res) {
            res.end();
            queryResponse("SELECT time FROM food_runs WHERE food_run_id=$1", [id], res, function(res, affected_rows, data) {
                var mail = new Email({
                    to:fields['email'],
                    from:'info@stlfoodrescue.org',
                    subject:'St. Louis Food Rescue Food-Run Confirmation'
                });
                mail.addSubVal('-time-', moment(affected_rows[0]['time']).format("dddd, MMMM Do, YYYY [at] h:mma"));
                mail.addSubVal('-people-', fields['volunteer_amount']);
                mail.addSubVal('-cars-', fields['car_amount']);
                mail.html = 'You have edited your existing reservation for the delivery on <strong>-time-</strong>. Our records now show that you will be bringing <strong>-people- volunteers</strong> and <strong>-cars- vehicles</strong>.<br /><br />Please do not hesitate to contact our Volunteer Coordinator, Will Hopkins, at (314) 445-7273 if you have any questions.';
                sendgrid.send(mail, function (success, message) {
                    if (!success) {
                        util.log(message);
                    }
                });
            });
        });
    },
    getFoodRunInvoice:function (id, res) {
        var qs = "SELECT v.first_name, v.last_name, map.volunteer_amount, map.car_amount " +
            "FROM food_runs fr, volunteers_food_runs_map map, volunteers v " +
            "WHERE fr.food_run_id = map.food_run_id AND map.volunteer_id = v.volunteer_id AND fr.food_run_id=$1";
        var data = [id];
        queryResponse(qs, data, res, function(res, affected_rows, data) {
            res.writeHead(200, {'Content-Type':'text/plain'});
            res.write(JSON.stringify(affected_rows));
            res.end();
        });
    },
    register:function (server) {
        server.get('/data/foodrun', function (req, res) {
            api.getFoodRuns(res);
        });

        server.get('/data/foodrun/filter/week1', function (req, res) {
            api.getFoodRunsFiltered(0, res);
        });

        server.get('/data/foodrun/filter/week2', function (req, res) {
            api.getFoodRunsFiltered(1, res);
        });

        server.get('/data/foodrun/:id', function (req, res) {
            api.getFoodRun(req.params.id, res);
        });

        server.get('/data/foodrun/:id/volunteer', function (req, res) {
            api.getTotalVolunteerAmount(req.params.id, res);
        });

        server.get('/data/foodrun/:id/car', function (req, res) {
            api.getTotalCarAmount(req.params.id, res);
        });

        server.post('/data/foodrun', function (req, res) {
            api.addFoodRun(req.body, res);
        });

        server.del('/data/foodrun/:id', function (req, res) {
            api.removeFoodRun(req.params.id, res);
        });

        server.put('/data/foodrun/:id', function (req, res) {
            api.editFoodRun(req.params.id, req.body, res);
        });

        server.post('/data/foodrun/:id/volunteer', function (req, res) {
            api.addVolunteerAssignment(req.params.id, req.body, res);
        });

        server.put('/data/foodrun/:id/volunteer', function (req, res) {
            api.editVolunteerAssignment(req.params.id, req.body, res);
        });

        server.get('/data/foodrun/:id/invoice', function (req, res) {
            api.getFoodRunInvoice(req.params.id, res);
        });
    }
};

exports.api = api;