import random
from datetime import date
from datetime import datetime
import random

def createFoodRuns (pickup, dropoff, when):
 template = """INSERT INTO food_runs (pick_up_location, drop_off_location, time)
VALUES (%i, %i, '%s');""" % (pickup, dropoff, when)
 return template

def createVolunteers (email, success, failure):
 template = """INSERT INTO volunteers (email, success, failure)
VALUES ('%s', %i, %i);""" % (email, success, failure)
 return template

def createMap (volunteer_id, food_run_id, volunteer_amount, car_amount):
 template = """INSERT INTO volunteers_food_runs_map (volunteer_id, food_run_id, volunteer_amount, car_amount)
VALUES (%i, %i, %i, %i);""" % (volunteer_id, food_run_id, volunteer_amount, car_amount)
 return template

def randomString(words, low, high):
    s = ""
    length = random.randint(low, high)
    for w in range(0, length):
        s += random.choice(words)+" "
    s = s.replace("'", "").replace('"', '')
    return s

def randomDate():
    start_date = datetime.today().replace(day=1).toordinal()
    month = datetime.today().month;
    year = datetime.today().year;
    if month == 12:
        month = 0
        year = year + 1
    else:
        month += 1;
    end_date = datetime.today().replace(day=1, month=month).toordinal()
    return datetime.fromordinal(random.randint(start_date, end_date)).replace(hour=random.randint(8, 23)).replace(minute=random.randint(0, 59));

words = []
for line in open('/usr/share/dict/words'):
    words.append(str(line).strip())

for i in range(0, 30):
    email = randomString(words, 1, 2)
    success = random.randint(1, 10)
    failure = random.randint(1, 10)
    print createVolunteers(email, success, failure)

print "------------------------------------------------"

for i in range(0, 50):
    pickup = random.randint(1, 3)
    dropoff = random.randint(1, 3)
    when = randomDate()
    print createFoodRuns(pickup, dropoff, when)

print "------------------------------------------------"

for i in range(0, 150):
    volunteer_id = random.randint(1, 30)
    food_run_id = random.randint(1, 50)
    volunteer_amount = random.randint(1, 2)
    car_amount = random.randint(1, 2)
    print createMap(volunteer_id, food_run_id, volunteer_amount, car_amount)