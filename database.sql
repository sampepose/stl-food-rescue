--Schema

DROP TABLE if EXISTS volunteers CASCADE;
DROP TABLE if EXISTS food_runs CASCADE;
DROP TABLE if EXISTS volunteers_food_runs_map CASCADE;
DROP SEQUENCE if EXISTS food_runs_food_run_id_seq;
DROP SEQUENCE if EXISTS volunteers_volunteer_id_seq;

CREATE TABLE volunteers (
       volunteer_id 		 serial	      PRIMARY KEY,
       first_name        varchar(50)  NOT NULL      DEFAULT '',
       last_name         varchar(50)  NOT NULL      DEFAULT '',
       phone_number      varchar(14)  NOT NULL      DEFAULT '',
       email		         varchar(50) 	NOT NULL      UNIQUE,
       success 		       integer	    NOT NULL      DEFAULT 0,
       failure 		       integer	    NOT NULL      DEFAULT 0
       sent_first_survey boolean	    NOT NULL      DEFAULT '0'
);

--We will use a map of integers to represent locations. Varchar would be too messy to parse.
CREATE TABLE food_runs (
       food_run_id        serial                      PRIMARY KEY,
       pick_up_location   integer                     NOT NULL,
       drop_off_location  integer                     NOT NULL,
       time               timestamp    NOT NULL
);

CREATE TABLE volunteers_food_runs_map (
       volunteer_id      integer            references volunteers(volunteer_id) ON DELETE CASCADE,
       food_run_id      integer        references food_runs(food_run_id) ON DELETE CASCADE,
       volunteer_amount integer,
       car_amount integer,
       PRIMARY KEY(volunteer_id, food_run_id)
);