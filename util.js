var pg = require('pg');
var os = require('os');
var url = require('url');
if (!process.env.PORT)
    vars = require("./vars").vars;
else
    vars = [];
var io;

//Postgres database connection
var pgClient = new pg.Client(vars.DATABASE_URL || process.env.DATABASE_URL);
pgClient.connect();

//Function to produce log output
var log = function (header, message) {
    if (true) {
        console.log(header);
        if (message)
            console.log(message);
        console.log("------------------------------------------------");
    }
};

//Used to send information from database query back to client
//TODO: Make this better....instead of passing in success/error funcs as parameters (requiring certain order),
// pass them in like you pass into all other async methods where it overrides default methods
var queryResponse = function (qs, data, res, func, err) {
    var query;
    if (data) {
        query = pgClient.query(qs, data);
    } else {
        query = pgClient.query(qs);
    }
    log("Database Query:", query.text);
    var affected_rows = [];
    query.on('row', function (row) {
        affected_rows.push(row);
    });
    query.on('end', function (result) {
        if (func) {
            func(res, affected_rows, data);
        }
        else if (affected_rows.length > 0) {
            res.writeHead(200, {'Content-Type':'text/plain'});
            if (affected_rows.length == 1)
                res.write(JSON.stringify(affected_rows[0]));
            else
                res.write(JSON.stringify(affected_rows));
            res.end();
        } else {
            res.end();
        }
    });
    query.on('error', function (error) {
        if (err) {
            err(error, res);
            func = false;
        } else {
            log("Server Response:", error + "\n\tfrom: " + query.text + '\n');
            res.writeHead(200, {'Content-Type':'text/plain'});
            res.write(error + "\n \t from: " + query.text + '\n');
            res.end();
        }
    });
};

//Get host computers IP address
var getIp = function () {
    var interfaces = os.networkInterfaces();
    var addresses = [];
    for (var conType in interfaces) {
        for (var a in interfaces[conType]) {
            var address = interfaces[conType][a];
            if (address.family == 'IPv4' && !address.internal) {
                addresses.push(address.address);
            }
        }
    }
    return addresses;
};

var validateEmail = function (x) {
    var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b/
    return re.test(x);
};

exports.queryResponse = queryResponse;
exports.log = log;
exports.getIp = getIp;
exports.validateEmail = validateEmail;
exports.io = io;