var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var food_run_api = require('./food_run_api').api;
var volunteer_api = require('./volunteer_api').api;
var util = require('./util');
util.io = require('socket.io').listen(server);
util.io.configure(function () {
    util.io.set("transports", ["xhr-polling"]);
    util.io.set("polling duration", 10);
    util.io.set('log level', 1);
});
var vars;
if (!process.env.PORT)
    vars = require("./vars").vars;
else
    vars = [];
var MemoryStore = require('connect').session.MemoryStore;
var sessionStorage = new MemoryStore({ reapInterval:60000 * 10 });

// Configuration
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.locals({layout:false, pretty:true});
app.use({
    _:require("underscore")
});
app.use(express.cookieParser("this is a random string for encryption"));
app.use(express.session({ store:sessionStorage, cookie:{ maxAge:3600000 }}));

//Register APIs
food_run_api.register(app);
volunteer_api.register(app);

app.get('/', function (req, res) {
    res.render('index', {page:"index"});
});

app.get('/admin', function (req, res) {
    if (req.session.admin)
        res.render('admin');
    else
        res.render('adminLogin');
});

app.post('/admin/login', function (req, res) {
    if (req.body.username == '' || req.body.password == '') {
        res.render('adminLogin', {error_message:"Blank username or password."});
        return;
    }
    var pass = vars.ADMIN_DASHBOARD_PASS || process.env.ADMIN_DASHBOARD_PASS;
    if (req.body.username === "admin" && req.body.password === process.env.ADMIN_DASHBOARD_PASS) {
        req.session.admin = true;
        res.redirect('admin');
    } else {
        res.render('adminLogin', {error_message:"Incorrect username and password combination."});
    }
});

var port = process.env.PORT || 8080;
server.listen(port);

util.log("STLFR:", "Running at " + util.getIp() + " on port: " + port);
