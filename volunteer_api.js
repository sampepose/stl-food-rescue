var squel = require('squel');
var util = require('./util');

var queryResponse = util.queryResponse;

var api = {
    getVolunteers:function (res) {
        var qs = "SELECT v.*, " +
            "(SELECT MAX(fr.time) FROM food_runs fr, volunteers_food_runs_map map " +
            "WHERE fr.food_run_id = map.food_run_id AND map.volunteer_id = v.volunteer_id GROUP BY v.volunteer_id) AS last_delivery_date " +
            "FROM volunteers v;";
        queryResponse(qs, [], res);
    },
    getVolunteer:function (id, res) {
        var qs = "SELECT v.*, " +
            "(SELECT MAX(fr.time) FROM food_runs fr, volunteers_food_runs_map map " +
            "WHERE fr.food_run_id = map.food_run_id AND map.volunteer_id = v.volunteer_id AND v.volunteer_id=$1 GROUP BY v.volunteer_id) AS last_delivery_date " +
            "FROM volunteers v WHERE v.volunteer_id=$1;";
        queryResponse(qs, [id], res);
    },
    addVolunteer:function (fields, res) {
        var data = [], n = 1;
        var qs = squel.insert({usingValuePlaceholders:true})
            .into('volunteers');
        for (var k in fields) {
            if (isNaN(fields[k]) || fields[k] === '') {
                data.push(fields[k]);
                qs.set(k, "$" + n++);
            } else {
                data.push(parseInt(fields[k]));
                qs.set(k, "$" + n++);
            }
        }
        qs = qs.toString() + " RETURNING *";
        queryResponse(qs, data, res);
    },
    removeVolunteer:function (id, res) {
        var qs = "DELETE FROM volunteers WHERE volunteer_id=$1";
        queryResponse(qs, [id], res);
    },
    editVolunteer:function (id, fields, res) {
        var data = [id], n = 2;
        var qs = squel.update({usingValuePlaceholders:true})
            .table('volunteer')
            .where('volunteer_id=$1');
        for (var k in fields) {
            if (fields[k] == null) {
                qs.set(k, "NULL");
            } else if (isNaN(fields[k]) || fields[k] === '') {
                data.push(fields[k]);
                qs.set(k, "$" + n++);
            } else {
                data.push(parseInt(fields[k]));
                qs.set(k, "$" + n++);
            }
        }
        qs = qs.toString();
        queryResponse(qs, data, res);
    },
    checkVolunteer:function (fields, res) {
        if (!fields || !fields['email']) {
            res.end();
            return;
        }
        var qs = "SELECT * FROM volunteers WHERE email ilike $1 LIMIT 1"
        queryResponse(qs, [fields['email']], res);
    },
    register:function (server) {
        server.get('/data/volunteer', function (req, res) {
            api.getVolunteers(res);
        });

        server.get('/data/volunteer/:id', function (req, res) {
            api.getVolunteer(req.params.id, res);
        });

        server.post('/data/volunteer', function (req, res) {
            api.addVolunteer(req.body, res);
        });

        server.del('/data/volunteer/:id', function (req, res) {
            api.removeVolunteer(req.params.id, res);
        });

        server.put('/data/volunteer/:id', function (req, res) {
            api.editVolunteer(req.params.id, req.body, res);
        });

        server.post('/data/volunteer/check', function (req, res) {
            api.checkVolunteer(req.body, res);
        });
    }
};

exports.api = api;