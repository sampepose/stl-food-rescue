var nodemailer = require('nodemailer');
var vars;
if (!process.env.PORT)
    vars = require("../vars").vars;
else
    vars = [];
var pg = require('pg');
var pgClient = new pg.Client(vars.DATABASE_URL || process.env.DATABASE_URL);
pgClient.connect();

// Create an Amazon SES transport object
var transport = nodemailer.createTransport("SES", {
    AWSAccessKeyID: vars.AMAZON_KEY || process.env.AMAZON_KEY,
    AWSSecretKey: vars.AMAZON_SECRET  || process.env.AMAZON_SECRET
});

var queryResponse = function (qs, func, err) {
    var query;
    query = pgClient.query(qs);
    var affected_rows = [];
    query.on('row', function (row) {
        affected_rows.push(row);
    });
    query.on('end', function (result) {
        if (func)
            func(affected_rows);
    });
    query.on('error', function (error) {
        if (err)
            err(error);
        func = false;
        console.log("PG ERROR: " + error);
    });
};

var sendSurveys = function() {
    var qs = "SELECT v.*, " +
        "(SELECT MAX(fr.time) FROM food_runs fr, volunteers_food_runs_map map " +
        "WHERE fr.food_run_id = map.food_run_id AND map.volunteer_id = v.volunteer_id GROUP BY v.volunteer_id) AS last_delivery_date " +
        "FROM volunteers v;";
    counter++;
    queryResponse(qs, function(data) {
        counter--;
        for (var i = 0; i < data.length; i++) {
            if (data[i].last_delivery_date != null && !data[i].sent_first_survey) {
                var message = {
                    from: 'St. Louis Food Rescue <info@stlfoodrescue.org>',
                    to: "'" + data[i].first_name + " " +  data[i].last_name + "' <" + data[i].email  +">",
                    subject: 'Survey Email',//TODO: Change
                    text: 'Survey Email Message',//TODO: Change
                    html:'<p>Survey Email Message</p>'//TODO: Change
                };
                (function(i, orig){
                    counter++;
                    transport.sendMail(message, function(error) {
                        counter--;
                        if (error){
                            console.log("Error occurred: " + error.message);
                            process.nextTick(endHandler);
                        } else {
                            counter++;
                            queryResponse("UPDATE volunteers SET sent_first_survey='1' WHERE volunteer_id=" + data[i].volunteer_id, function(data) {
                                counter--;
                                process.nextTick(endHandler);
                            });
                        }
                    });
                })(i, data)
            }
        }
    });
};
var counter = 0;

sendSurveys();

var endHandler = function() {
    if (counter == 0)
        process.exit();
};
