var getFirstDayOfWeek = function (d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
};

var validateEmail = function (x) {
    var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|us)\b/;
    return re.test(x);
};

var FoodRunModel = Backbone.Model.extend({
    initialize:function () {
        if (this.get("total_volunteer_amount") == null) {
            this.set("total_volunteer_amount", 0);
        }
        if (this.get("total_car_amount") == null) {
            this.set("total_car_amount", 0);
        }
    },
    defaults:{
        food_run_id:"",
        pick_up_location:"",
        drop_off_location:"",
        time:"",
        total_volunteer_amount:"",
        total_car_amount:""
    },
    idAttribute:"food_run_id",
    urlRoot:'/data/foodrun/'
});

var FoodRunCollection = Backbone.Collection.extend({
    model:FoodRunModel,
    url:"/data/foodrun/"
});

var VolunteerModel = Backbone.Model.extend({
    defaults:{
        volunteer_id:"",
        first_name:"",
        last_name:"",
        phone_number:"",
        email:"",
        success:"0",
        failure:"0",
        last_delivery_date:"",
        sent_first_survey:"0"
    },
    idAttribute:"volunteer_id",
    urlRoot:'/data/volunteer/'
});

var VolunteerCollection = Backbone.Collection.extend({
    model:VolunteerModel,
    url:"/data/volunteer/"
});

var FoodRunColl = new FoodRunCollection();
var VolunteerColl = new VolunteerCollection();

var VolunteerView = Backbone.View.extend({
    template:_.template($("#volunteer").html()),
    addNewVolunteer:function () {
        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var phone = $("#phoneNumber").val();
        var email = $("#email").val();
        if (validateEmail(email)) {
            var n = new VolunteerModel({first_name: firstName, last_name:lastName, phone_number:phone,email:email});
            delete n.id;
            delete n.attributes.volunteer_id;
            delete n.attributes.last_delivery_date;
            n.save({}, {async:false});
            VolunteerColl.fetch();
            alert("New volunteer added!");
            $("#newVolunteer").val('');
        } else {
            alert("Not a valid email.");
        }
    },
    initialize:function () {
        this.collection.bind("reset", this.render, this);
        this.collection.bind("add", this.render, this);
        this.collection.bind("remove", this.render, this);
    },
    render:function () {
        $('#inject').html(this.template());//this.model.toJSON()
        for (var i = 0; i < this.collection.length; i++) {
            new VolunteerRowView({model:this.collection.at(i)}).render();
        }
        $("#newSubmit").click(this.addNewVolunteer);
    }
});

var VolunteerRowView = Backbone.View.extend({
    template:_.template($("#volunteer-row").html()),
    events:{
        "click":"remove"
    },
    render:function () {
        $('#volunteerList').append(this.template(this.model.toJSON()));
        this.setElement($("#" + this.model.id));
    },
    remove:function () {
        var answer = confirm("Delete " + this.model.get("email") + "?");
        if (answer) {
            var res = prompt("Confirmation: Type \"confirm\" to delete the volunteer.");
            if (res === "confirm") {
                this.model.destroy();
                this.unbind("click");
                this.$el.remove();
                alert("Volunteer removed!");
            } else {
                alert("Volunteer removal aborted!");
            }
        }

    }
});

var FoodRunView = Backbone.View.extend({
    template:_.template($("#foodrun").html()),
    addNewFoodRun:function () {
        var time = new XDate($("#date").val() + " " + $("#time").val()).toString("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
        if (!$("#date").val()) {
            alert("Select a date!");
            return;
        }
        var dropoff = $("#drop-off").val();
        var pickup = $("#pick-up").val();
        var n = new FoodRunModel({time:time, pick_up_location:pickup, drop_off_location:dropoff});
        delete n.id;
        delete n.attributes.food_run_id;
        delete n.attributes.total_car_amount;
        delete n.attributes.total_volunteer_amount;
        n.save({}, {async:false});
        FoodRunColl.fetch();
        alert("New delivery added!");
    },
    initialize:function () {
        this.collection.bind("reset", this.render, this);
        this.collection.bind("add", this.render, this);
        this.collection.bind("remove", this.render, this);
    },
    render:function () {
        $('#inject').html(this.template());//this.model.toJSON()
        for (var i = 0; i < this.collection.length; i++) {
            new FoodRunRowView({model:this.collection.at(i)}).render();
        }
        $("#date").datepicker({ minDate:0, showWeek:true, firstDay:1});
        $("#newSubmit").click(this.addNewFoodRun);
    }
});

var FoodRunRowView = Backbone.View.extend({
    template:_.template($("#foodRun-row").html()),
    events:{
        "click":"remove"
    },
    render:function () {
        $('#foodRunList').append(this.template(this.model.toJSON()));
        this.setElement($("#" + this.model.id));
    },
    remove:function () {
        var answer = confirm("Delete this delivery (highly unrecommended...)?");
        if (answer) {
            var res = prompt("Confirmation: Type \"confirm\" to delete the delivery.");
            if (res === "confirm") {
                this.model.destroy();
                this.unbind("click");
                this.$el.remove();
                alert("Delivery removed!");
            } else {
                alert("Delivery removal aborted!");
            }
        }
    }
});

var InvoiceView = Backbone.View.extend({
    week1:[],
    week2:[],
    template:_.template($("#invoice").html()),
    initialize:function () {
        var self = this;
        $.ajax({
            type:'GET',
            url:'/data/foodrun/filter/week1',
            dataType:'json',
            timeout:10000,
            async:false,
            success:function (data) {
                self.week1 = data;
            },
            error:function (xhr, type) {
                alert('Ajax error!');
            }
        });
        $.ajax({
            type:'GET',
            url:'/data/foodrun/filter/week2',
            dataType:'json',
            timeout:10000,
            async:false,
            success:function (data) {
                self.week2 = data;
            },
            error:function (xhr, type) {
                alert('Ajax error!');
            }
        });
    },
    render:function () {
        $('#inject').html(this.template());
        var day1 = new XDate(getFirstDayOfWeek(new Date().setHours(0, 0, 0, 0)), true);
        $("#week1Header").html(day1.toString("MMM. d") + "-" + day1.clone().addDays(6).toString("MMM. d"));
        day1.addWeeks(1);
        $("#week2Header").html(day1.toString("MMM. d") + "-" + day1.addDays(6).toString("MMM. d"));
        if (this.week1)
            for (var i = 0; i < this.week1.length; i++) {
                this.addOne(i, this.week1, "week1");
            }
        if (this.week2)
            for (var i = 0; i < this.week2.length; i++) {
                this.addOne(i, this.week2, "week2");
            }
    },
    addOne:function (i, arr, el) {
        var id = arr[i]['food_run_id'];
        var containerTemp = _.template($("#invoice-foodrun-container").html());
        $("#" + el).append(containerTemp({time:arr[i]['time'], drop_off_location:arr[i]['drop_off_location']}));
        $.ajax({
            type:'GET',
            url:'/data/foodrun/' + id + '/invoice',
            dataType:'json',
            timeout:10000,
            async:false,
            success:function (data) {
                if (data.length >= 1)
                    for (var j = 0; j < data.length; j++) {
                        new InvoiceFoodRunView({el:el, food_run_id:id, first_name:data[j]['first_name'],last_name:data[j]['last_name'], car_amount:data[j]['car_amount'], volunteer_amount:data[j]['volunteer_amount']}).render();
                    }
                else
                    $('#' + el).append("No volunteers signed up.");
            },
            error:function (xhr, type) {
                alert('Ajax error!');
            }
        });
    }
});

var InvoiceFoodRunView = Backbone.View.extend({
    template:_.template($("#invoice-foodrun").html()),
    render:function () {
        $('#' + this.options.el).append(this.template(this.options));
    }
});

var AppRouter = Backbone.Router.extend({
    routes:{
        "":"changeContent",
        "volunteer":"volunteerPage",
        "foodrun":"foodRunPage",
        "invoice":"invoicePage",
        "*path":"error"
    },
    changeContent:function () {
        var id = "#" + (Backbone.history.fragment == "" ? "homepage" : Backbone.history.fragment);
        $("#inject").html(_.template($(id).html()));
    },
    volunteerPage:function () {
        VolunteerColl.fetch({async:false});
        new VolunteerView({collection:VolunteerColl}).render();
    },
    foodRunPage:function () {
        FoodRunColl.fetch({async:false});
        new FoodRunView({collection:FoodRunColl}).render();
    },
    invoicePage:function () {
        new InvoiceView().render();
    },
    error:function () {
        this.navigate("#");
        this.changeContent();
    }
});

$(document).ready(function () {
    new AppRouter;
    Backbone.history.start();
});