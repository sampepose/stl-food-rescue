var client_auth = [];

client_auth.login = {
    submit:function () {
        var required = ['#username', '#password'];
        return client_auth.utils.validate_form(required);
    }
};

client_auth.utils = {
    validate_form:function (required) {
        var ret = true;
        for (var i = 0; i < required.length; i++) {
            var good = ret;
            var dom = $(required[i]);
            if (dom.val() == '') {
                ret = this.display_error(dom, "This cannot be left blank.");
            } else if (dom.attr('type') === 'email' && !this.validate_email(dom.val())) {
                ret = this.display_error(dom, "Please enter a valid email.");
            } else if (dom.attr('id') === 'password' && dom.closest("form#login").length <= 0) {
                if (dom.val().length < 6) {
                    ret = false;
                } else if (dom.val().length > 50) {
                    ret = false;
                } else if ($('#email').length != 0 && $('#email').val().indexOf(dom.val()) != -1) {
                    ret = this.display_error(dom, "Your email cannot contain your password.");
                } else if ($('#display_name').length != 0 && $('#display_name').val().replace(/\s/g, '').toLowerCase().indexOf(dom.val().replace(/\s/g, '').toLowerCase()) != -1) {
                    ret = this.display_error(dom, "Your display name cannot contain your password.");
                } else {
                    var bad = ['123456', '1234567', '12345678', '123456789', '1234567890', 'password', 'secret', 'liverpool', 'letmein', 'qwerty', 'iloveyou', 'princess', 'abc123'];
                    for (var j = 0; j < bad.length; j++) {
                        var s = bad[j].replace(/\s/g, '').toLowerCase();
                        var pass = dom.val().replace(/\s/g, '').toLowerCase();
                        if (s.indexOf(pass) != -1) {
                            ret = this.display_error(dom, "This password is too common.");
                        }
                    }
                }
                if (good) {
                    $("#err_" + dom.attr('id')).remove();
                    dom.removeAttr('style');
                }
            } else {
                $("#err_" + dom.attr('id')).remove();
                dom.removeAttr('style');
            }
        }
        return ret;
    },
    validate_email:function (x) {
        var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|us)\b/;
        return re.test(x);
    },
    display_error:function (el, err) {
        if ($('#err_' + el.attr('id')).length <= 0) {
            el.after("<p class=\'red\' id=\'err_" + el.attr('id') + "\'>Error: " + err + "</p>");
        } else {
            $('#err_' + el.attr('id')).html("<p class=\'red\' id=\'err_" + el.attr('id') + "\'>Error: " + err + "</p>");
        }
        el.css("border", "1px solid red");
        return false;
    }
};